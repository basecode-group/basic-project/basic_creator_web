import React, { Component } from 'react';
import { Card, Row, Col, Button, Input, Alert, Table, Tag, Popconfirm, Icon, Typography, message, Modal } from 'antd';
import CreatorAdd from "@/pages/creator/components/CreatorAdd";
import CreatorEdit from "@/pages/creator/components/CreatorEdit";
import AxiosUtil from "@/tool/AxiosUtil";

const { Column } = Table;
const { Paragraph } = Typography;

/**
 * 代码生成
 *
 * @author zhangby
 * @date 18/2/20 12:18 pm
 */
class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      rows: [],
      pagination: {
        pageSize: 10,
        current: 1,
        total: 0,
        defaultPageSize: 10,
        onChange: (page, pageSize) => {
          this.state.query.pageNum = page;
          this.state.query.pageSize = pageSize;
          // eslint-disable-next-line react/no-access-state-in-setstate
          this.setState(this.state);
          this.queryCreatorList();
        },
      },
      loading: false,
      query: {
        keyword: '',
      },
    };
    this.queryCreatorList = this.queryCreatorList.bind(this);
  }

  // 初始化
  componentWillMount() {
    this.queryCreatorList();
  }

  // 查询列表
  queryCreatorList() {
    this.setState({ loading: true });
    AxiosUtil.get('/api/creator',this.state.query).then(res => {
      const val = res.result;
      this.setState({
        pagination: {
          pageSize: val.size,
          current: val.current,
          total: val.total,
        },
        list: val.records,
        loading: false,
      })
    }).catch(() => this.setState({ loading: false }));
  }

  // 批量删除
  // eslint-disable-next-line class-methods-use-this
  deleteBachConfirm() {
    const { rows } = this.state;
    // eslint-disable-next-line no-underscore-dangle
    const _this = this;
    Modal.confirm({
      title: '确定要删除选择的记录吗?',
      content: `已选择${rows.length}项记录。`,
      onOk() {
        AxiosUtil.delete(`/api/creator/${rows.join(',')}`).then(() => {
          message.success('删除奖期成功');
          _this.queryCreatorList();
          _this.setState({ rows: [] });
        });
      },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  deleteConfirm(id) {
    AxiosUtil.delete(`/api/creator/${id}`).then(() => {
      message.success('删除代码生成器成功');
      this.queryCreatorList();
    })
  }

  // 生成代码
  generator() {
    const { rows } = this.state;
    // eslint-disable-next-line no-underscore-dangle
    const _this = this;
    Modal.confirm({
      title: '确定要生成代码吗?',
      content: `已选择${rows.length}项记录。`,
      onOk() {
        AxiosUtil.post(`/api/creator/generator/${rows.join(',')}`).then(() => {
          message.success('生成代码成功');
          _this.queryCreatorList();
          _this.setState({ rows: [] });
        });
      },
    });
  }

  render() {
    return (
      <div >
        <Card>
          <Row style={{ marginBottom: 30 }}>
            <Col span={12}>
              <CreatorAdd queryCreatorList={this.queryCreatorList}/>
              {/* 隐藏功能 */}
              {
                this.state.rows.length > 0 ?
                  <span>
                    <Button type="danger" icon="delete" style={{ marginLeft: 10 }}
                            onClick={this.deleteBachConfirm.bind(this)}>批量删除</Button>

                    <Button className="ant-btn-green" icon="printer" style={{ marginLeft: 10 }}
                            onClick={this.generator.bind(this)}>生成代码</Button>
                  </span>
                  : null
              }
            </Col>
            <Col span={12} style={{ textAlign: 'right' }}>
              <Input.Search placeholder="模糊查询" style={{ maxWidth: 450 }} onSearch={val => {
                this.state.query.keyword = val;
                // eslint-disable-next-line react/no-access-state-in-setstate
                this.setState(this.state);
                this.queryCreatorList();
              }} enterButton />
            </Col>
          </Row>
          {/* 已选择 */}
          <Alert style={{ margin: '20px 0 20px 0' }}
                 message={
                   <div>
                     已选择 <span style={{
                     color: '#1690ff',
                     fontWeight: 600,
                     margin: '0 5px',
                   }}>{this.state.rows.length}</span> 项
                     <span style={{
                       color: '#1690ff',
                       marginLeft: 30,
                       cursor: 'pointer',
                     }} onClick={() => this.setState({ rows: [] })}>清空</span>
                   </div>
                 } type="info" showIcon/>
          {/* 列表 */}
          <Table
            dataSource={this.state.list}
            scroll={{ x: 900 }}
            loading={this.state.loading}
            rowSelection={{
              columnWidth: 40,
              selectedRowKeys: this.state.rows,
              onChange: selectedRowKeys => {
                this.setState({ rows: selectedRowKeys })
              },
            }}
            rowKey="id">
            {/* eslint-disable-next-line react/jsx-no-bind */}
            <Column width={100} align="left" title="标签名" dataIndex="name" render={this.titleRender.bind(this)}/>
            <Column align="center" title="表名" dataIndex="tableName" render={this.tableRender}/>
            <Column width={100} align="center" title="创建人" dataIndex="author" render={this.authorRender}/>
            <Column align="center" title="输出路径" dataIndex="outPutDir" render={this.renderText}/>
            <Column align="center" title="包名" dataIndex="packageDir" render={this.renderText}/>
            <Column align="center" title="创建时间" dataIndex="createDate" render={this.renderText}/>
            {/* eslint-disable-next-line @typescript-eslint/no-unused-vars */}
            <Column align="center" title="操作" dataIndex="id" render={(id, record) => (
              <div>
                <CreatorEdit queryCreatorList={this.queryCreatorList} creatorId={id} />
                {/* 删除 */}
                <Popconfirm
                  placement="topRight"
                  onConfirm={this.deleteConfirm.bind(this, id)}
                  title="确定要删除记录吗？"
                  icon={<Icon type="question-circle-o" style={{ color: 'red' }} />}
                >
                  <Button
                    style={{ marginLeft: 10 }}
                    type="danger"
                    shape="circle"
                    icon="delete"
                  />
                </Popconfirm>
              </div>
            )}/>
          </Table>
        </Card>
      </div>
    )
  }

  titleRender(val, record) {
    return (
      <CreatorEdit queryCreatorList={this.queryCreatorList} creatorId={record.id} >
        <Button type="link" style={{ paddingLeft: 0 }}>{val}</Button>
      </CreatorEdit>
    )
  }

  // 创建日期
  // eslint-disable-next-line class-methods-use-this,react/sort-comp
  renderText(val) {
    return <Tag.CheckableTag >{val}</Tag.CheckableTag>;
  }

  // eslint-disable-next-line class-methods-use-this
  authorRender(val) {
    return <Tag color="volcano">{val}</Tag>
  }

  // eslint-disable-next-line class-methods-use-this
  tableRender(val) {
    return <Paragraph ellipsis style={{ marginBottom: 0 }}>{val}</Paragraph>
  }
}

export default index;
